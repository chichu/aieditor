# 字体配置

## 字体配置

```javascript
{
    fontFamily:{
        values:[
            {name: "默认字体", value: ""},
            {name: "宋体", value: "SimSun"},
            {name: "仿宋", value: "FangSong"},
            {name: "黑体", value: "SimHei"},
            {name: "楷体", value: "KaiTi"},
            {name: "微软雅黑", value: "Microsoft YaHei"},
            {name: "方正仿宋简体_GBK", value: "FangSong_GB2312"},
            {name: "Arial", value: "Arial"},
        ]
    }
}
```



